// Configuration des variables d'environnement grâce à dotenv
require('dotenv').config();

// Import du module Node 'http' permettant de créer un server Web
const http = require('http');
// Import du module url permettant de découper (parser) notre req.url
const url = require('url');
// Import du module querystring permettant de parser les datas de notre POST
const querystring = require('querystring');
// Import du module ejs permettant de générer du html
const ejs = require('ejs');
// Import du module path permettant de faciliter la génération de chemins
const path = require('path');
// Import du module file system pour gérer les fichiers
const fs = require('fs');

//Création du serveur
const app = http.createServer((req, res) => {
    //Récupération des infos de la route (dans req)
    //console.log(req);
    //url de la requête :
    const requestUrl = url.parse(req.url).pathname;
    //query de la requête si présents
    const requestQuery = url.parse(req.url).query;
    //method de la requête (get ou post)
    const requestMethod = req.method;

    console.log(requestUrl);
    console.log(requestQuery);
    console.log(requestMethod);

    //Gestion du dossier public et des différents types de fichiers
    //Création du path
    const filePublic = path.resolve('public' + requestUrl)
    console.log('Searched file : ', filePublic);
    //Si la route n'est pas "/" et si le fichier existe bien
    if( requestUrl !== '/' && fs.existsSync(filePublic)) {
        //On lit le fichier
        const file = fs.readFileSync(filePublic);
        // console.log(file);
        //On récupère l'extension du fichier
        //path.extname(pathFichier) -> renvoie l'extension du fichier
        //on fait un replace, pour enlever le . devant l'extension
        const extension = path.extname(filePublic).replace('.', '');
        console.log(extension);

        //Selon l'extension : traitement
        let contentType = '';
        //Est-ce que extension existe dans le tableau ?
        if(['gif', 'png', 'jpeg', 'jpg', 'bmp', 'webp', 'svg'].includes(extension)) {
            contentType = 'image/' + extension;
        }
        else if(extension === 'css') {
            contentType = 'text/css';
        }

        //Envoi de la réponse
        res.writeHead(200, {
            "Content-type" : contentType
        });
        //On envoie le fichier dans la réponse,
        // si c'était une image, elle sera interprétée comme telle puisque dans Content-type il y'a 'image/[extension]'
        // si c'était du css, il sera interprété comme tel puisque dans Content-type, il y'a 'text/css'
        res.end(file);
        return;
    }


    //Définition des différents comportements en fonction des routes(url) et de la méthode
    if (requestUrl === '/' && requestMethod === 'GET') {
        //Home Page
        console.log('Bienvenue sur la Home Page !');

        // Création des données à afficher
        const today = new Date().toLocaleDateString('fr-be', { dateStyle: 'long' })
        const trainers = [
            { firstname: 'Aude', lastname: 'Beurive' },
            { firstname: 'Pierre', lastname: 'Santos' },
            { firstname: 'Aurélien', lastname: 'Strimelle' }
        ]

        // Utilisation d'ejs pour rendre la vue
        // Génération du path
        const filename = path.resolve('views', 'home.ejs');
        // Création des datas à envoyer à la vue
        const data = { today, trainers }; //On founit un objet qui contient today et notre tableau de formateurs
        // Rendu de la vue
        //renderFile :
            //1er param : chemin vers le fichier ejs
            //2eme : Data à lui envoyer
            //3eme : Callback (err, render) => {} 
                //err -> si erreur lors du rendu (fichier introuvable, autre)
                //render -> si pas d'erreur, le rendu se trouve dans render
        //A partir du chemin, des datas, nous renvoie une chaine qui contient tout le html à afficher
        ejs.renderFile(filename, data, (err, render) => {
            //Si erreur dans le rendu
            if (err) {
                console.log(err);
                return;
            }
            console.log("Rendu Home Page effectué !");
            //console.log(render);
            //Si pas d'erreur, on peut envoyer la réponse à la requête
            // Envoi de la réponse (res -> response)
            //Contenant la "vue"
            res.writeHead(200, {
                "Content-type": "text/html" //On précise qu'on renvoie du html
            })
            res.end(render); //On termine la requête en fournissant les données à afficher dans la réponse
        })
    }
    else if (requestUrl === '/contact') {
        if (requestMethod === 'GET') {
            //Contact Page
            console.log('Bienvenue sur la Contact Page');

            // Rendu ejs
            //Récupération du chemin vers le fichier ejs
            const filename = path.resolve('views', 'contact.ejs');
            //Rendu
            ejs.renderFile(filename, (err, render) => {
                if(err) {
                    console.log(err);
                    return;
                }
                console.log('Rendu de la page Contact !');
                // Envoi de la réponse
                res.writeHead(200, {
                    "Content-type": "text/html"
                })
                res.end(render);
            })

        }
        else if (requestMethod === 'POST') {
            //Récupération du form Contact
            //Récupération des données du formulaire
            let data = '';
            //event déclenché à la réception des datas
            req.on('data', (form) => {
                console.log("Form : ", form);
                data += form.toString('utf-8');
                console.log("Data : ", data);
            });
            //event déclenché après avoir reçu toutes les données
            req.on('end', () => {
                //Traitement des données
                console.log('End : ', data);
                // Convertion des données (grâce à )
                const result = querystring.parse(data);
                console.log('Data after parsing : ', result);
                //C'est ensuite à partir de cet objet, qu'on fait insert en db

                //Traitement de la response (res) 
                //Redirection vers la page de notre choix
                res.writeHead(301, {
                    "Location": "/" //Renverra vers la homepage
                });
                res.end();
            })
        }
    }
    else if ( requestUrl === '/about' && requestMethod === 'GET') {
        const filename = path.resolve('views', 'about.ejs');
        
        const person = {
            firstname : 'Aude',
            lastname : 'Beurive',
            gender : 'F',
            birthdate : new Date(1989, 9, 16),
            lovedCourses : ['NodeJs', 'Algo', 'Angular']
        }

        const data = { person }

        ejs.renderFile(filename, data, (err, render) => {
            if(err) {
                console.log(err);
                return;
            }
            res.writeHead(200, {
                "Content-type": "text/html"
            });
            res.end(render);    
        })



    }
    else {
        //route inexistante
        //Page 404
        const filename = path.resolve('views', 'notfound.ejs');
        ejs.renderFile(filename, (err, render) => {
            if(err) {
                console.log(err);
                return;
            }
            console.log('Erreur 404');
            res.writeHead(200, {
                "Content-type": "text/html"
            });
            res.end(render);            
        })
    }
})

//Lancement du serveur
app.listen(process.env.PORT, () => {
    console.log(`Server started on port:${process.env.PORT}`);
})